php-parser (5.4.0-3) unstable; urgency=medium

  * Modernize PHPUnit syntax
  * Exclude test failing with PHPUnit 12
  * Revert "Force system dependencies loading (tests)"
  * Simplify build

 -- David Prévot <taffit@debian.org>  Mon, 17 Feb 2025 20:08:05 +0100

php-parser (5.4.0-2) unstable; urgency=medium

  * Source-only upload for testing migration

 -- David Prévot <taffit@debian.org>  Wed, 01 Jan 2025 12:50:17 +0100

php-parser (5.4.0-1) unstable; urgency=medium

  [ Ondrej Mirtes ]
  * Promoted properties with hooks do not need visibility modifier
  * Add flags helper methods `Property::isAbstract()` and `Property::isFinal()`
  * Add `PropertyHook::isFinal()` helper method with tests
  * Emit error - Multiple properties cannot share the same hooks

  [ Nikita Popov ]
  * Make PropertyHook::getStmts() less incorrect
  * Fix PropertyHook::getStmts() for set hook
  * Release PHP-Parser 5.4.0

 -- David Prévot <taffit@debian.org>  Wed, 01 Jan 2025 12:14:31 +0100

php-parser (5.3.1-2) unstable; urgency=medium

  * Source-only upload for testing migration

 -- David Prévot <taffit@debian.org>  Sun, 13 Oct 2024 09:23:14 +0100

php-parser (5.3.1-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Support PropertyHooks in NameResolver
  * Include trailing semicolon in GroupUse
  * Add basic support for tab indentation
  * Avoid negative indentation in formatting-preserving printer
  * Support declaring functions with name exit/die
  * Release PHP-Parser 5.3.1

 -- David Prévot <taffit@debian.org>  Wed, 09 Oct 2024 15:39:29 +0100

php-parser (5.2.0-2) unstable; urgency=medium

  * Source-only upload

 -- David Prévot <taffit@debian.org>  Mon, 16 Sep 2024 08:17:52 +0200

php-parser (5.2.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Declare PHP 8.4 as the newest supported version
  * [8.4] Add support for __PROPERTY__ magic constant
  * [8.4] Add support for property hooks
  * Add support for PHP 8.4 exit function
  * [8.4] Add support for asymmetric visibility modifiers
  * Release PHP-Parser 5.2.0

  [ Ruud Kamphuis ]
  * Normalize enum value to ClassConstFetch

 -- David Prévot <taffit@debian.org>  Mon, 16 Sep 2024 07:38:31 +0200

php-parser (5.1.0-1) unstable; urgency=medium

  [ Markus Staab ]
  * Declare more precise phpdoc types (#993)

  [ Nikita Popov ]
  * [8.4] Add support for new deref without parens
  * Fix ternary precedence printing
  * Release PHP-Parser 5.1.0

  [ Jorg Adam Sowa ]
  * Adjust tests to be compatible with PHPUnit 10 (#998)

  [ David Prévot ]
  * Drop patches not needed anymore

 -- David Prévot <taffit@debian.org>  Tue, 23 Jul 2024 15:07:14 +0900

php-parser (5.0.2-3) unstable; urgency=medium

  * Drop abstract test class and its dependencies (Closes: #1070583)
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sun, 19 May 2024 17:00:14 +0200

php-parser (5.0.2-2) unstable; urgency=medium

  * Source only upload

 -- David Prévot <taffit@debian.org>  Sun, 10 Mar 2024 15:21:29 +0100

php-parser (5.0.2-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Avoid cyclic reference in parser
  * Fix indentation detection after opening tag
  * Release PHP-Parser 5.0.2

  [ Maarten Buis ]
  * Update `PhpVersion::getNewestSupported()` to PHP 8.3

  [ David Prévot ]
  * Force system dependencies loading (tests)

 -- David Prévot <taffit@debian.org>  Thu, 07 Mar 2024 23:28:20 +0100

php-parser (5.0.1-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Check for tokens with non-integer ID
  * Release PHP-Parser 5.0.1

  [ David Prévot ]
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Sat, 24 Feb 2024 09:29:35 +0100

php-parser (5.0.0-1) unstable; urgency=medium

  * Upload to unstable

  [ Ondrej Mirtes ]
  * Fix parent class of PropertyItem and UseItem

  [ Nikita Popov ]
  * Avoid PHPUnit deprecation warnings
  * Release PHP-Parser 5.0.0

  [ David Prévot ]
  * Extend clean
  * Drop versioned build-dependencies

 -- David Prévot <taffit@debian.org>  Wed, 10 Jan 2024 08:41:20 +0100

php-parser (5.0.0~rc1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Don't parse unicode escapes for PHP < 7.0
  * Add support for dumping additional attributes
  * Add rawValue to InterpolatedStringPart and doc strings
  * Introduce Stmt\Block
  * Remove Stmt\Throw
  * Use visitor to assign comments
  * Remove ParserFactory::create()
  * Fix handling of empty input
  * Release PHP-Parser 5.0.0rc1

  [ Abdul Malik Ikhsan ]
  * Improve performance of find() and findFirst() when passed $nodes is empty array

  [ David Prévot ]
  * Compatibility with recent PHPUnit (10), partially addresses #1039814

 -- David Prévot <taffit@debian.org>  Thu, 21 Dec 2023 13:20:06 +0100

php-parser (5.0.0~beta1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Add upgrading notes for lexer changes
  * Release PHP-Parser 5.0.0 Beta 1

  [ David Prévot ]
  * Override new Makefile

 -- David Prévot <taffit@debian.org>  Tue, 19 Sep 2023 09:38:12 +0530

php-parser (5.0.0~alpha3-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Add support for typed constants
  * Support readonly anonymous classes
  * Add support for NodeVisitor::REPLACE_WITH_NULL
  * Support CRLF newlines in pretty printer
  * Release PHP-Parser 5.0.0-alpha3

 -- David Prévot <taffit@debian.org>  Sun, 25 Jun 2023 18:22:18 +0200

php-parser (5.0.0~alpha2-1) experimental; urgency=medium

  [ Nikita Popov ]
  * [PHP 8.3] Support dynamic class const fetch
  * Update upgrading documentation
  * Release PHP-Parser 5.0.0 alpha 2

  [ David Prévot ]
  * Update standards version to 4.6.2, no changes needed.

 -- David Prévot <taffit@debian.org>  Mon, 06 Mar 2023 13:24:17 +0100

php-parser (5.0.0~alpha1-1) experimental; urgency=medium

  * Upload alpha to experimental

  [ Nikita Popov ]
  * Drop support for running on PHP 7.0
  * Use PHP 8.0 token representation
  * Remove PHP 5 parser
  * Release PHP-Parser 5.0.0-alpha1

  [ David Prévot ]
  * Revert "Track version 4 for now (Bookworm?)"

 -- David Prévot <taffit@debian.org>  Fri, 09 Sep 2022 17:25:57 -0400

php-parser (4.15.1-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Support readonly as function name
  * Add support for true type
  * Fix length bounds check in Name::slice()
  * Fix empty list insertion of multiple attributes
  * Release PHP-Parser 4.15.1

  [ Anton ]
  * Add __serialize/__unserialize to ClassMethod::$magicNames

  [ George Peter Banyard ]
  * Add support for DNF types (#862)

  [ David Prévot ]
  * Track version 4 for now (Bookworm?)

 -- David Prévot <taffit@debian.org>  Fri, 09 Sep 2022 17:14:41 -0400

php-parser (4.14.0-1) unstable; urgency=medium

  [ Marijn van Wezel ]
  * Reflect support for PHP 8.1 in the README

  [ Tomas Votruba ]
  * [LNumber] Add rawValue attribute to LNumber to allow numeric separator etc.
  * [DNumber] Add rawValue attribute to hold the original value (#833)
  * [String_] Add rawValue attribute (#831)
  * [PHP 8.2] Add readonly class support (#834)

  [ Nikita Popov ]
  * Release PHP-Parser 4.14.0

  [ David Prévot ]
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sat, 18 Jun 2022 17:26:14 +0200

php-parser (4.13.2-1) unstable; urgency=medium

  [ Jaroslav Hanslík ]
  * Added builders for enum and enum case

  [ Nikita Popov ]
  * Release PHP-Parser 4.13.2

 -- David Prévot <taffit@debian.org>  Mon, 06 Dec 2021 09:49:28 -0400

php-parser (4.13.1-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.13.1

 -- David Prévot <taffit@debian.org>  Thu, 04 Nov 2021 12:57:00 -0400

php-parser (4.13.0-2) unstable; urgency=medium

  * Allow stderr during CI

 -- David Prévot <taffit@debian.org>  Thu, 07 Oct 2021 15:30:31 -0400

php-parser (4.13.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.13.0

  [ David Prévot ]
  * Simplify gbp import-orig
  * Update standards version to 4.6.0, no changes needed.
  * Install /u/s/p/{autoloaders,overrides} files
  * Generate phpabtpl at build time
  * Use dh-sequence-phpcomposer instead of pkg-php-tools

 -- David Prévot <taffit@debian.org>  Thu, 07 Oct 2021 14:17:02 -0400

php-parser (4.10.4-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.10.4

  [ David Prévot ]
  * Workaround --include-path broken(?) in PHPUnit 9

 -- David Prévot <taffit@debian.org>  Mon, 21 Dec 2020 10:01:27 -0400

php-parser (4.10.3-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.10.3

  [ David Prévot ]
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Wed, 09 Dec 2020 06:21:24 -0400

php-parser (4.10.2-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.10.2

 -- David Prévot <taffit@debian.org>  Sat, 26 Sep 2020 08:40:07 -0400

php-parser (4.10.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.10.0

 -- David Prévot <taffit@debian.org>  Tue, 22 Sep 2020 07:44:40 -0400

php-parser (4.9.1-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.9.1

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)

 -- David Prévot <taffit@debian.org>  Tue, 01 Sep 2020 14:40:39 -0400

php-parser (4.9.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.9.0

 -- David Prévot <taffit@debian.org>  Mon, 24 Aug 2020 10:40:31 -0400

php-parser (4.8.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Allow PHPUnit 9
  * Release PHP-Parser 4.8.0

 -- David Prévot <taffit@debian.org>  Mon, 17 Aug 2020 18:31:58 +0200

php-parser (4.7.0-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Add ParentConnectingVisitor and NodeConnectingVisitor (#681)

  [ Graham Campbell ]
  * Corrected license text

  [ Tomas Votruba ]
  * [PHP 8.0] Add match expressions (#672)

  [ Nikita Popov ]
  * [PHP 8.0] Support trailing comma in closure use list
  * Release PHP-Parser 4.7.0

  [ David Prévot ]
  * Update copyright
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Thu, 30 Jul 2020 05:37:49 +0200

php-parser (4.6.0-1) unstable; urgency=medium

  [ TomasVotruba ]
  * [PHP 8.0] Add trailing comma in parameter list
  * Add constructor promotion support

  [ Nikita Popov ]
  * Release PHP-Parser 4.6.0

  [ David Prévot ]
  * Install upstream doc without extra doc/

 -- David Prévot <taffit@debian.org>  Mon, 06 Jul 2020 14:56:54 +0200

php-parser (4.5.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.5.0

  [ David Prévot ]
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test

 -- David Prévot <taffit@debian.org>  Fri, 05 Jun 2020 17:25:11 -1000

php-parser (4.4.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.4.0

  [ David Prévot ]
  * Drop versioned dependency satisfied in (old)stable
  * Update Standards-Version to 4.5.0
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.

 -- David Prévot <taffit@debian.org>  Wed, 15 Apr 2020 09:29:15 -1000

php-parser (4.3.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.3.0

 -- David Prévot <taffit@debian.org>  Sun, 10 Nov 2019 17:18:19 -1000

php-parser (4.2.5-1) unstable; urgency=medium

  [ Kyle ]
  * Optimize production build by ignoring dev files

  [ Nikita Popov ]
  * Release PHP-Parser 4.2.5

  [ David Prévot ]
  * Document gbp import-ref usage
  * Update standards version to 4.4.1, no changes needed.
  * Set upstream metadata fields: Repository.

 -- David Prévot <taffit@debian.org>  Tue, 29 Oct 2019 11:32:40 -1000

php-parser (4.2.4-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.2.4

  [ Tomas Votruba ]
  * add getProperties() and getConstants() to ClassLike
  * add getTraitUses() method to ClassLike

  [ David Prévot ]
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- David Prévot <taffit@debian.org>  Mon, 02 Sep 2019 18:12:05 -1000

php-parser (4.2.3-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Make compatible with PhpUnit 8
  * Release PHP-Parser 4.2.3

  [ Tomáš Votruba ]
  * [PHP 7.4] Add support for numeric literal separators (#615)

 -- David Prévot <taffit@debian.org>  Tue, 13 Aug 2019 18:14:55 -1000

php-parser (4.2.2-2) unstable; urgency=medium

  * Upload to unstable in sync with phpdox
  * Compatibility with recent PHPUnit (Closes: #929065)
  * Update standards version, no changes needed.
  * Set upstream metadata fields: Contact, Name.

 -- David Prévot <taffit@debian.org>  Fri, 09 Aug 2019 16:23:29 -1000

php-parser (4.2.2-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.2.2

  [ David Prévot ]
  * Update help2man call

 -- David Prévot <taffit@debian.org>  Tue, 11 Jun 2019 15:01:43 -1000

php-parser (4.2.1-1) experimental; urgency=medium

  [ Tomas Votruba ]
  * Add support for ??= operator

  [ Nikita Popov ]
  * Release PHP-Parser 4.2.1

  [ David Prévot ]
  * Provide homemade autoload for tests

 -- David Prévot <taffit@debian.org>  Mon, 18 Feb 2019 10:13:10 -1000

php-parser (4.2.0-2) experimental; urgency=medium

  * Drop incorrect dependencies from autoload.php

 -- David Prévot <taffit@debian.org>  Mon, 21 Jan 2019 11:45:03 -1000

php-parser (4.2.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.2.0

  [ David Prévot ]
  * Use debhelper-compat 12

 -- David Prévot <taffit@debian.org>  Mon, 14 Jan 2019 21:00:24 -1000

php-parser (4.1.1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.1.1

  [ David Prévot ]
  * Update Standards-Version to 4.3.0

 -- David Prévot <taffit@debian.org>  Sat, 29 Dec 2018 09:35:08 +1030

php-parser (4.1.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.1.0

  [ David Prévot ]
  * Simplify a bit d/README.source

 -- David Prévot <taffit@debian.org>  Fri, 12 Oct 2018 16:39:47 -1000

php-parser (4.0.4-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0.4

  [ David Prévot ]
  * Use debhelper-compat 11
  * Use https in Format
  * Get rid of https target
  * Update Standards-Version to 4.2.1

 -- David Prévot <taffit@debian.org>  Thu, 20 Sep 2018 19:48:01 -1000

php-parser (4.0.3-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0.3

  [ David Prévot ]
  * Update Standards-Version to 4.1.5

 -- David Prévot <taffit@debian.org>  Wed, 18 Jul 2018 17:41:19 +0800

php-parser (4.0.2-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0.2

  [ David Prévot ]
  * Update Standards-Version to 4.1.4

 -- David Prévot <taffit@debian.org>  Tue, 05 Jun 2018 15:32:12 -1000

php-parser (4.0.1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0.1

 -- David Prévot <taffit@debian.org>  Wed, 28 Mar 2018 16:45:56 -1000

php-parser (4.0.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0.0

  [ Gabriel Caruso ]
  * Support PHPUnit 7

 -- David Prévot <taffit@debian.org>  Sun, 04 Mar 2018 19:35:43 -1000

php-parser (4.0.0~beta1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0 Beta 1

  [ Miguel Piedrafita ]
  * Update license year

  [ David Prévot ]
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.3
  * Update copyright (years)
  * Drop bootstrap file removed upstream

 -- David Prévot <taffit@debian.org>  Tue, 27 Feb 2018 21:59:46 -1000

php-parser (4.0.0~alpha3-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0 Alpha 3

 -- David Prévot <taffit@debian.org>  Thu, 28 Dec 2017 16:19:45 +0530

php-parser (4.0.0~alpha2-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 4.0.0 Alpha 2

  [ David Prévot ]
  * Install upstream documentation in its doc directory
  * Workaround phpab issue
  * Update Standards-Version to 4.1.2

 -- David Prévot <taffit@debian.org>  Sun, 03 Dec 2017 14:21:44 -1000

php-parser (3.1.2-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Add attribute for namespace kinds (#417)
  * Preserve comments on empty blocks (#382)
  * Add setDocComment() to namespace build (#437)
  * Release PHP-Parser 3.1.2

 -- David Prévot <taffit@debian.org>  Sat, 18 Nov 2017 21:23:00 -1000

php-parser (3.1.1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 3.1.1

  [ David Prévot ]
  * Drop now useless entries for ci
  * Revert "Acknowledge now provided --help option"
  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Thu, 12 Oct 2017 15:15:30 -1000

php-parser (2.1.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Remove internal LNumber::parse() method
  * Document that XDebug is super slow
  * Release PHP-Parser 2.1.0

  [ Remi Collet ]
  * support -h and --help standard options

  [ David Prévot ]
  * Update Standards-Version to 3.9.8
  * Acknowledge now provided --help option

 -- David Prévot <taffit@debian.org>  Tue, 19 Apr 2016 21:21:53 -0400

php-parser (2.0.1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Distinguish declare(); and declare(){}
  * Support hashbang before namespace declaration
  * Update semi-reserved keyword list
  * Fix __halt_compiler() pretty printing edge case
  * Release PHP-Parser 2.0.1

  [ David Prévot ]
  * Update Standards-Version to 3.9.7

 -- David Prévot <taffit@debian.org>  Wed, 02 Mar 2016 17:11:34 -0400

php-parser (2.0.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Introduce Scalar\EncapsedStringPart
  * Release PHP-Parser 2.0.0

 -- David Prévot <taffit@debian.org>  Sat, 05 Dec 2015 16:33:33 -0400

php-parser (2.0.0~beta1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Add php-parse as composer bin
  * Fix autoloader path in php-parse
  * Release version 2.0.0 beta 1

  [ Dmitry Patsura ]
  * Use composer PSR-4 autoloader

  [ David Prévot ]
  * Adapt to php-parse renamed upstream
  * Provide homemade static autoload.php
  * Use static autoloader instead of dynamic one

 -- David Prévot <taffit@debian.org>  Fri, 23 Oct 2015 15:52:12 -0400

php-parser (2.0.0~alpha1-1) experimental; urgency=medium

  * Upload alpha to experimental

  [ Nikita Popov ]
  * Move parser to Parser\Php5
  * Fork separate PHP 7 parser
  * Add Multiple parser
  * Add basic ParserFactory
  * Release PHP-Parser 2.0 Alpha 1

  [ David Prévot ]
  * Revert "Track stable releases"
  * Install new upgrade notice
  * Don’t force php5 anymore
  * Force testing of the local lib
  * Adapt CI to new distribution

 -- David Prévot <taffit@debian.org>  Sun, 06 Sep 2015 15:38:40 -0400

php-parser (1.4.0-1) unstable; urgency=medium

  [ Nikita Popov ]
  * Prepare PHP-Parser 1.4.0 release

  [ David Prévot ]
  * Make help output help2man-friendly
  * Track stable releases

 -- David Prévot <taffit@debian.org>  Wed, 29 Jul 2015 18:41:29 +0200

php-parser (1.3.0-1) unstable; urgency=medium

  * Upload to unstable since Jessie has been released

  [ Nikita Popov ]
  * Release PHP-Parser 1.3.0

  [ Gerrit Addiks ]
  * added column-numbers to syntax errors

 -- David Prévot <taffit@debian.org>  Sun, 03 May 2015 08:49:24 -0400

php-parser (1.2.2-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 1.2.2

  [ David Prévot ]
  * Drop the .php file from /usr/bin

 -- David Prévot <taffit@debian.org>  Sat, 04 Apr 2015 17:47:06 -0400

php-parser (1.2.1-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Strict type compliance
  * Release PHP-Parser 1.2.1

 -- David Prévot <taffit@debian.org>  Wed, 25 Mar 2015 09:58:43 -0400

php-parser (1.2.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Revert "Create .gitattributes with basic export-ignores"
  * Release PHP-Parser 1.2.0

  [ David Prévot ]
  * Revert "Do not ship no longer provided documentation"
  * Revert "Disable tests (no longer provided)"
  * Simplify DEP-8 call

 -- David Prévot <taffit@debian.org>  Tue, 24 Mar 2015 16:16:10 -0400

php-parser (1.1.0-1) experimental; urgency=medium

  [ Nikita Popov ]
  * Release PHP-Parser 1.1

  [ David Prévot ]
  * Disable tests (no longer provided)
  * Do not ship no longer provided documentation

 -- David Prévot <taffit@debian.org>  Thu, 22 Jan 2015 12:26:48 -0400

php-parser (1.0.2-1) experimental; urgency=medium

  * Upload to experimental to respect the freeze

  [ Arne Blankerts ]
  * Make NameResolver resolve trait alias and precedence names

  [ nikic ]
  * Release PHP-Parser 1.0.2

  [ David Prévot ]
  * Add a get-orig-source target

 -- David Prévot <taffit@debian.org>  Fri, 07 Nov 2014 15:04:53 -0400

php-parser (1.0.1-1) unstable; urgency=medium

  [ nikic ]
  * Disallow new without a class name
  * Fix var_dump truncation with xdebug in php-parse.php
  * Add ability to pass code directly to php-parse.php
  * Release version 1.0.1

  [ David Prévot ]
  * Improve manual page generation

 -- David Prévot <taffit@debian.org>  Thu, 16 Oct 2014 22:04:02 -0400

php-parser (1.0.0-2) unstable; urgency=medium

  * Install bootstrap, as needed by phpdox
  * Make sure DEP-8 test are actually about the installed package
  * Bump standards version to 3.9.6

 -- David Prévot <taffit@debian.org>  Mon, 29 Sep 2014 17:29:33 -0400

php-parser (1.0.0-1) unstable; urgency=medium

  [ nikic ]
  * Remove "experimental" message
  * Remove deprecated Template and TemplateLoader
  * Add migration guide for 0.9 -> 1.0
  * Release PHP-Parser 1.0.0

  [ David Prévot ]
  * Display upstream version instead of Debian one
  * Install upgrade documentation
  * Upload stable version to unstable

 -- David Prévot <taffit@debian.org>  Fri, 12 Sep 2014 15:09:52 -0400

php-parser (1.0.0~beta2-1) experimental; urgency=medium

  [ nikic ]
  * Add experimental php-parse script
  * Release PHP-Parser 1.0.0 Beta 2

  [ Nikita Popov ]
  * Make autoloader for new names PSR-0 compliant

  [ David Prévot ]
  * No tests if DEB_BUILD_OPTIONS contains nocheck
  * Revert "Remove failing tests"
  * Install CLI
  * Add manpage for CLI

 -- David Prévot <taffit@debian.org>  Thu, 04 Sep 2014 16:38:31 -0400

php-parser (1.0.0~beta1-1) experimental; urgency=medium

  [ nikic ]
  * Release PHP-Parser 1.0.0 Beta 1

  [ David Prévot ]
  * Update packaging to Composer source, and upload to experimental
  * Remove failing tests

 -- David Prévot <taffit@debian.org>  Thu, 12 Jun 2014 14:36:41 -0400

php-parser (0.9.4-1) unstable; urgency=low

  * Initial Release (Closes: #746431)

 -- David Prévot <taffit@debian.org>  Tue, 29 Apr 2014 18:52:39 -0400
